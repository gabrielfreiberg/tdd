|----------------------------------------------------------------------------------------------------|
|                                            IMPORTANT!!!!                                           |
|----------------------------------------------------------------------------------------------------|
Before you can use TDD testing tool, you need to create custom object in you Business Manager.

1) Go to "Administration > Site Development > Import & Export"
2) Upload the file located at app_test/site_template/meta/tdd-custom-objecttype-definitions.xml
3) Import the file as "Meta Data"
