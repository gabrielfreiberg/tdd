TDD Framework
==================

If you're interested in writing more robust code you should really consider writing tests for your code. TDD or Test Driven Development is a principle where you write the test first and then write your code.  It's a common practice among professional developers and now you can do TDD on the Demandware platform with the "TDD Framework for Demandware".

**Main Screen**
> ![list-of-test-suites.png](https://bitbucket.org/repo/nAdrBM/images/3005906755-list-of-test-suites.png)

**Test Suite Screen**
> ![cipherhelpertestsuite.png](https://bitbucket.org/repo/nAdrBM/images/3391737330-cipherhelpertestsuite.png)